/* template strings on ecma 2015*/
let name = "Ana"
let concat = "Hello " + name + "!"
/** You can use N lines with template strings */

let template = `
   Hello
   ${name}!
`
console.log(concat, template)
console.log(`1 + 1 = ${1+1}`)
let up = upText => upText.toLowerCase() // arrow function example
console.log(`Heyyy...${up('Jonn')}, be careful!`)