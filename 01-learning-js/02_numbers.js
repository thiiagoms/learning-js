const peso1 = 1.0
const peso2 = Number('2.0')

console.log(peso1, peso2, typeof peso1, typeof peso2)
console.log(Number.isInteger(peso1))

const aval1 = 9.874
const aval2 = 8.874


const total = (aval1 * peso1) + (aval2  * peso2)
const media = total / (peso1 + peso2)

console.log(media.toFixed(2))
console.log(media.toString(2))
